from django.db import models

from products.constatns import MeasureType


class Product(models.Model):
    name = models.CharField(max_length=50)
    measure_type = models.IntegerField(choices=MeasureType.MEASURE_TYPES, default=MeasureType.UNIT)

    class Meta:
        db_table = 'product'
        unique_together = ("name", "measure_type")
