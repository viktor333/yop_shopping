from rest_framework import generics

from products.models import Product
from products.serializers import ProductSerializer


class CreateProductApiView(generics.CreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
