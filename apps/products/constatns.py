from django.utils.translation import ugettext_lazy as _


class MeasureType:
    L = 0
    KG = 1
    G = 2
    UNIT = 3

    MEASURE_TYPES = (
        (L, _('l')),
        (KG, _('Kg')),
        (G, _('g')),
        (UNIT, _('Unit')),
    )
