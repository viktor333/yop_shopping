from django.urls import path

from . import views


app_name = 'users'


urlpatterns = [
    path('login/', views.LoginApiView.as_view()),
    path('<int:pk>/friends/', views.GetUserFriendsListApiView.as_view()),
]
