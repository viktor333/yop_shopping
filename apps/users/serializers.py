from django.contrib.auth import get_user_model
from rest_framework import serializers

from users.models import UserFriendsConnection

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email')


class UserFriendsSerializer(serializers.ModelSerializer):
    friend = UserSerializer(read_only=True)

    class Meta:
        model = UserFriendsConnection
        fields = ('status', 'friend')
