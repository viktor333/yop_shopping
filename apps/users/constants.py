from django.utils.translation import ugettext_lazy as _


class FriendshipStatus:
    INCOMING = 0
    OUTCOMMING = 1
    FRIENDS = 2

    FRIENDSHIP_STATUSES = (
        (INCOMING, _('Incoming')),
        (OUTCOMMING, _('Outcomming')),
        (FRIENDS, _('Friends')),
    )
