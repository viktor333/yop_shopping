# Generated by Django 2.1.5 on 2019-01-20 14:45

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20190120_0944'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserFriendsConnection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.IntegerField(choices=[(0, 'Incoming'), (1, 'Outcomming'), (2, 'Friends')], default=1)),
                ('friend', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='friends', to=settings.AUTH_USER_MODEL)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='userfriend',
            name='friend',
        ),
        migrations.RemoveField(
            model_name='userfriend',
            name='user',
        ),
        migrations.DeleteModel(
            name='UserFriend',
        ),
    ]
