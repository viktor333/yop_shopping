import json
import jwt

from django.contrib.auth import get_user_model, authenticate

from rest_framework import views
from rest_framework.response import Response
from rest_framework import generics

from users.models import UserFriendsConnection
from users.serializers import UserSerializer, UserFriendsSerializer

User = get_user_model()


class LoginApiView(views.APIView):
    permission_classes = []
    authentication_classes = []

    def post(self, request, *args, **kwargs):
        if not request.data:
            return Response({'Error': "Please provide email/password"}, status=400)
        email = request.data.get('email')
        password = request.data.get('password')
        try:
            user = authenticate(request, email=email, password=password)
        except User.DoesNotExist:
            return Response({'Error': "Invalid email/password"}, status=400)
        if user:
            payload = {'id': user.id, 'email': user.email}
            token = jwt.encode(payload, "SECRET_KEY").decode('utf-8')
            user.token = token
            user.save()
            response = {'user': UserSerializer(user).data, 'token': token}
            return Response(json.dumps(response), status=200, content_type="application/json")
        else:
            return Response(json.dumps({'Error': "Invalid credentials"}), status=400, content_type="application/json")


class GetUserFriendsListApiView(generics.ListAPIView):
    serializer_class = UserFriendsSerializer
    lookup_url_kwarg = 'pk'

    def get_queryset(self):
        return UserFriendsConnection.objects.filter(
            owner=self.kwargs.get(self.lookup_url_kwarg)
        ).select_related('friend')
