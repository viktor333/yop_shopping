from django.utils.translation import ugettext_lazy as _


class PurchaseStatus:
    INPROGRESS = 0
    COMPLETED = 1
    PARTIAL_COMPLETED = 2

    PURCHASE_STATUSES = (
        (INPROGRESS, _('InProgress')),
        (COMPLETED, _('Completed')),
        (PARTIAL_COMPLETED, _('PartialCompleted')),
    )


class PurchasePriority:
    LOW = 0
    MEDIUM = 1
    HIGH = 2

    PURCHASE_PRIORITIES = (
        (LOW, _('Low')),
        (MEDIUM, _('Medium')),
        (HIGH, _('High')),
    )
