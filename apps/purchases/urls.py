from django.urls import path

from . import views

app_name = 'purchases'

urlpatterns = [
    path('', views.UserPurchasesListApiView.as_view()),
    path('create/', views.PurchaseCreateApiView.as_view()),
    path('<int:purchase_id>/', views.PurchaseRetrieveUpdateDeleteAPIView.as_view()),
    path('<int:purchase_id>/products/', views.CreatePurchaseProductApiView.as_view()),
    path('<int:purchase_id>/products/<int:pk>/', views.DeleteUpdatePurchaseProductApiView.as_view()),
]
