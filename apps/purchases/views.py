from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from rest_framework import generics
from rest_framework.response import Response

from products.models import Product
from purchases.models import Purchase, PurchaseProduct
from purchases.serializers import PurchaseSerializer, PurchaseProductSerializer


class UserPurchasesListApiView(generics.ListAPIView):
    serializer_class = PurchaseSerializer

    def get_queryset(self):
        return Purchase.objects.filter(Q(users__id=self.request.user.id) |
                                       Q(creator=self.request.user.id) &
                                       Q(deleted=False))


class PurchaseCreateApiView(generics.CreateAPIView):
    queryset = Purchase.objects.all()
    serializer_class = PurchaseSerializer


class PurchaseRetrieveUpdateDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Purchase.objects.all()
    serializer_class = PurchaseSerializer

    def destroy(self, request, *args, **kwargs):
        try:
            user_purchase = Purchase.objects.get(id=kwargs['purchase_id'], creator=request.user)
            user_purchase.deleted = True
            user_purchase.save()
            return Response(status=200)
        except ObjectDoesNotExist:
            return Response(status=404)


# set product_id of new or created product in serializer_data for creating or updating purchase_product instance
def prepare_purchase_product_data(data, purchase_id):
    product_data = data.get('product')
    product_instance, _ = Product.objects.get_or_create(name=product_data.get('name'),
                                                        measure_type=product_data.get('measure_type'))
    data['product_id'] = product_instance.id
    data['purchase'] = purchase_id


class CreatePurchaseProductApiView(generics.CreateAPIView):
    queryset = PurchaseProduct.objects.all()
    serializer_class = PurchaseProductSerializer

    def post(self, request, *args, **kwargs):
        prepare_purchase_product_data(request.data, kwargs['purchase_id'])
        serializer = PurchaseProductSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            serializer.save()
            return Response(serializer.data, status=200)
        else:
            return Response(serializer.errors, status=400)


class DeleteUpdatePurchaseProductApiView(generics.RetrieveDestroyAPIView):
    queryset = PurchaseProduct
    serializer_class = PurchaseProductSerializer

    def patch(self, request, *args, **kwargs):
        prepare_purchase_product_data(request.data, kwargs['purchase_id'])
        serializer = PurchaseProductSerializer(data=request.data, instance=PurchaseProduct.objects.get(id=kwargs['pk']))
        if serializer.is_valid(raise_exception=False):
            instance = serializer.save()
            not_purchased_products = PurchaseProduct.objects.filter(purchase=kwargs['purchase_id'],
                                                                    is_purchased=False).exists()
            purchased_products = PurchaseProduct.objects.filter(purchase=kwargs['purchase_id'],
                                                                is_purchased=True).exists()
            # set status of purchase completed if all products are purchased
            # and partial_completed if at least one product is purchased
            if not not_purchased_products:
                instance.purchase.status = PurchaseStatus.COMPLETED
                instance.save()
            elif purchased_products:
                instance.purchase.status = PurchaseStatus.PARTIAL_COMPLETED
                instance.save()
            return Response(serializer.data, status=200)
        else:
            return Response(serializer.errors, status=400)

    def destroy(self, request, *args, **kwargs):
        try:
            instance = PurchaseProduct.objects.get(id=kwargs['pk'], purchase=kwargs['purchase_id'])
            instance.deleted = True
            instance.save()
            return Response(status=200)
        except ObjectDoesNotExist:
            return Response(status=404)
