from django.contrib.auth import get_user_model
from django.db import models

from products.models import Product
from purchases.constants import PurchaseStatus, PurchasePriority

User = get_user_model()


class Purchase(models.Model):
    name = models.CharField(max_length=150)
    status = models.IntegerField(choices=PurchaseStatus.PURCHASE_STATUSES, default=PurchaseStatus.INPROGRESS)
    priority = models.IntegerField(choices=PurchasePriority.PURCHASE_PRIORITIES, default=PurchasePriority.MEDIUM)
    creation_time = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    deadline = models.DateTimeField(blank=True, null=True)
    creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_purchases')
    description = models.TextField(blank=True, null=True)
    users = models.ManyToManyField(User, related_name='purchases', blank=True)
    deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'purchase'
        unique_together = ("name", "description")


class PurchaseProductManager(models.Manager):

    def get_queryset(self):
        return super(PurchaseProductManager, self).get_queryset().filter(deleted=False)

    def with_deleted(self):
        return super(PurchaseProductManager, self).get_queryset()


class PurchaseProduct(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product_purchases')
    purchase = models.ForeignKey(Purchase, on_delete=models.CASCADE, related_name='purchase_products')
    amount = models.FloatField(blank=True, null=True)
    is_purchased = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)

    objects = PurchaseProductManager()
