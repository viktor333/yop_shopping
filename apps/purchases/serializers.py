from rest_framework import serializers

from products.models import Product
from products.serializers import ProductSerializer
from purchases.models import Purchase, PurchaseProduct
from users.serializers import UserSerializer


class PurchaseProductSerializer(serializers.ModelSerializer):
    product = ProductSerializer(read_only=True)
    product_id = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all(), write_only=True, source='product')

    class Meta:
        model = PurchaseProduct
        fields = '__all__'


class PurchaseSerializer(serializers.ModelSerializer):
    users = UserSerializer(many=True, read_only=True)
    purchase_products = PurchaseProductSerializer(many=True, read_only=True)

    class Meta:
        model = Purchase
        fields = (
            'id',
            'name',
            'status',
            'priority',
            'creation_time',
            'last_modified',
            'deadline',
            'creator',
            'description',
            'users',
            'purchase_products',
            'deleted',
        )

    def validate(self, attrs):
        print(self.context)
        return attrs
